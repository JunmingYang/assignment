﻿using System.Drawing;

namespace Assignment
{
    public class Circle : Shape
    {
        float Radius;
            
        public Circle() : base()
        {

        }

        public Circle(float x, float y, float Radius) : base(x, y)
        {
            this.Radius = Radius; 
        }

        public override void Set(params float[] list)
        {
            base.Set(list[0], list[1]);
            Radius = list[2];
        }

        public override void Draw(Graphics g)
        {
            Pen p = new Pen(Color.Red, 3);
            g.DrawEllipse(p, PenX, PenY, Radius * 2, Radius * 2);
        }

        public override string ToString()
        {
            return base.ToString() + "  Radius:" + Radius;
        }
    }
}
