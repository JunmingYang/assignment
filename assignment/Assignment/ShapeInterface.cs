﻿using System.Drawing;

namespace Assignment
{
    interface Shapes
    {
        void Set(params float[] list);
        void Draw(Graphics g);
    }
}
