﻿using System.Drawing;

namespace Assignment
{
    public class Rectangle : Shape
    {
        float Width, Height;

        public Rectangle() : base()
        {

        }

        public Rectangle(float x, float y, float Width, float Height) : base(x, y)
        {
            this.Width = Width;
            this.Height = Height;
        }

        public override void Set(params float[] list)
        {
            base.Set(list[0], list[1]);
            Width = list[2];
            Height = list[3];
        }

        public override void Draw(Graphics g)
        {
            Pen p = new Pen(Color.Green, 2);
            g.DrawRectangle(p, PenX, PenY, Width, Height);
        }

        public override string ToString()
        {
            return base.ToString() + "  Width:" + Width + "  Height:" + Height;
        }
    }
}
