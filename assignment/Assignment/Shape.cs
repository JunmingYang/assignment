﻿using System.Drawing;

namespace Assignment
{
    public abstract class Shape:Shapes
    {
        protected float PenX, PenY;

        public Shape()
        {

        }

        public Shape (float x, float y)
        {
            PenX = x;
            PenY = y;
        }

        public abstract void Draw(Graphics g);

        public virtual void Set(params float[] list)
        {
            PenX = list[0];
            PenY = list[1];
        }

        public override string ToString()
        {
            return base.ToString() + "  Point: [" + PenX + ", " + PenX + "]";
        }
    }
}
