﻿using System;

namespace Assignment
{
    class ShapeFactory
    {
        public Shape GetShape(string shapeType)
        {
            if (shapeType.Equals("Circle"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("Rectangle"))
            {
                  return new Rectangle();

            }
            else if (shapeType.Equals("Triangle"))
            {
                  return new Triangle();
            }
            else if (shapeType.Equals("Polygon"))
            {
                  return new Polygon();
            }
            else
            {
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}
