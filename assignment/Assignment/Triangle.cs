﻿using System.Drawing;

namespace Assignment
{
    public class Triangle : Shape
    {
        float Base, Adj;

        public Triangle() : base()
        {

        }

        public Triangle(float x, float y, float Base, float Adj) : base(x, y)
        {
            this.Base = Base;
            this.Adj = Adj;
        }

        public override void Set(params float[] list)
        {
            base.Set(list[0], list[1]);
            Base = list[2];
            Adj = list[3];
        }

        public override void Draw(Graphics g)
        {
            Pen p = new Pen(Color.Blue, 3);
            PointF[] points = { new PointF(PenX, PenY), new PointF(PenX + Base, PenY), new PointF(PenX, PenY + Adj) };
            g.DrawPolygon(p, points);
        }

        public override string ToString()
        {
            return base.ToString() + "  Base:" + Base + "  Adj:" + Adj;
        }
    }
}
