﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace Assignment
{
    public partial class Form1 : Form
    {
        ArrayList shapes = new ArrayList();
        private PointF Pen { get; set; }

        public Form1()
        {
            InitializeComponent();
            Pen = new Point(0, 0);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillRectangle(Brushes.White, pictureBox1.DisplayRectangle);
            ShapeFactory factory = new ShapeFactory();
            string program = textBox1.Text;
            string[] programLine = program.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < programLine.Length; i++)
                {
                    string line = programLine[i];
                    string[] command = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    if (command[0].Equals("MoveTo"))
                    {
                        try
                        {
                            string[] param = command[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            Pen = new PointF(float.Parse(param[0]), float.Parse(param[1]));
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("DrawTo"))
                    {
                        try
                        {
                            string[] param = command[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            Pen p = new Pen(Color.Black, 2);
                            PointF newPoint = new PointF(int.Parse(param[0]), int.Parse(param[1]));
                            g.DrawLine(p, Pen, newPoint);
                            Pen = newPoint;
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("Circle"))
                    {
                        try
                        {
                            string param = command[1];
                            param = param.Replace("<", "").Replace(">", "");
                            Shape s = factory.GetShape(command[0]);
                            s.Set(Pen.X, Pen.Y, float.Parse(param));
                            shapes.Add(s);
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("Rectangle"))
                    {
                        try
                        {
                            string param = command[1];
                            param = param.Replace("<", "").Replace(">", "");
                            string[] para = param.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            Shape s = factory.GetShape(command[0]);
                            s.Set(Pen.X, Pen.Y, float.Parse(para[0]), float.Parse(para[1]));
                            shapes.Add(s);
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("Triangle"))
                    {
                        try
                        {
                            string param = command[1];
                            param = param.Replace("<", "").Replace(">", "");
                            string[] para = param.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            Shape s = factory.GetShape(command[0]);
                            s.Set(Pen.X, Pen.Y, float.Parse(para[0]), float.Parse(para[1]));
                            shapes.Add(s);
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("Polygon"))
                    {
                        try
                        {
                            string param = command[1];
                            param = param.Replace("[", "").Replace("]", ""); 
                            string[] para = param.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            float[] Points = new float[para.Length + 2];
                            Points[0] = Pen.X;
                            Points[1] = Pen.Y;
                            for (int j = 2; j < Points.Length; j++)
                            {
                                Points[j] = float.Parse(para[j - 2]);
                            }
                            if (Points.Length % 2 == 0 && Points.Length >= 6)
                            {
                                Shape s = factory.GetShape(command[0]);
                                s.Set(Points);
                                shapes.Add(s);
                            }
                            else
                            {
                                textBox1.Text = "The number of coordinates should be an even number.";
                            }
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    if (command[0].Equals("Repeat"))
                    {
                        try
                        {
                            string Change = command[4].Replace("+", "");
                            int Count = int.Parse(command[1]);
                            if (command[2].Equals("Circle"))
                            {
                                float Radius = 0;
                                for (int j = 0; j < Count; j++)
                                {
                                    Shape s = factory.GetShape(command[2]);
                                    Radius += float.Parse(Change);
                                    s.Set(Pen.X - Radius, Pen.Y - Radius, Radius);
                                    shapes.Add(s);
                                }
                            }
                            if (command[2].Equals("Rectangle"))
                            {
                                float Height = 0, Width = 0;
                                for (int j = 0; j < Count; j++)
                                {
                                    Shape s = factory.GetShape(command[2]);
                                    Width += float.Parse(Change);
                                    Height += float.Parse(Change);
                                    s.Set(Pen.X - (Width / 2), Pen.Y - (Height / 2), Width, Height);
                                    shapes.Add(s);
                                }
                            }
                            if (command[2].Equals("Triangle"))
                            {
                                float Base = 0, Adj = 0;
                                for (int j = 0; j < Count; j++)
                                {
                                    Shape s = factory.GetShape(command[2]);
                                    Base += float.Parse(Change);
                                    Adj += float.Parse(Change);
                                    s.Set(Pen.X - (Base / (float)(2 + Math.Sqrt(2))), Pen.Y - (Base / (float)(2 + Math.Sqrt(2))), Base, Adj);
                                    shapes.Add(s);
                                }
                            }
                            if (command[2].Equals("Polygon"))
                            {
                                int SideCount = int.Parse(command[3]);
                                if (SideCount >= 3)
                                {
                                    float Radius = 0;
                                    for (int j = 0; j < Count; j++)
                                    {
                                        Shape s = factory.GetShape(command[2]);
                                        Radius += float.Parse(Change);
                                        double arc = 2 * Math.PI / SideCount;
                                        float[] Points = new float[SideCount * 2];
                                        for (int k = 0; k < SideCount * 2; k += 2)
                                        {
                                            double curArc = arc * k / 2;
                                            PointF pt = new PointF
                                            {
                                                X = Pen.X + (float)(Radius * Math.Cos(curArc)),
                                                Y = Pen.Y + (float)(Radius * Math.Sin(curArc))
                                            };
                                            Points[k] = pt.X;
                                            Points[k + 1] = pt.Y;
                                        }
                                        s.Set(Points);
                                        shapes.Add(s);
                                    }
                                }
                                else
                                {
                                    textBox1.Text = "Incorrect side count";
                                }    
                            }
                        }
                        catch (FormatException)
                        {
                            textBox1.Text = "Incorrect input format";
                        }
                        catch (IndexOutOfRangeException)
                        {
                            textBox1.Text = "Entered the wrong amount of data";
                        }
                    }

                    for (int j = 0; j < shapes.Count; j++)
                    {
                        Shape s;
                        s = (Shape)shapes[j];
                        if (s != null)
                        {
                            s.Draw(g);
                            Console.WriteLine(s.ToString());
                        }
                        else
                        {
                            Console.WriteLine("invalid shape in array");
                        }
                    }

            }
            
        }
    }
}
