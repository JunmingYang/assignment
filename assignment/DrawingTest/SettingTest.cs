﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;

namespace DrawingTest
{
    [TestClass]
    public class SettingTest
    {
        [TestMethod]
        public void Circle_ValidSetting()
        {
            float Radius = 50;
            float PenX = 0;
            float PenY = 0;
            Circle circle = new Circle();
            circle.Set(PenX, PenY, Radius);
            Assert.AreEqual(circle.ToString(), "Assignment.Circle  Point: [" + PenX + ", " + PenY + "]  Radius:" + Radius);
        }

        public void Rectangle_ValidSetting()
        {
            float Width = 50;
            float Height = 50;
            float PenX = 50;
            float PenY = 50;
            Rectangle rectangle = new Rectangle();
            rectangle.Set(PenX, PenY, Width, Height);
            Assert.AreEqual(rectangle.ToString(), "Assignment.Rectangle  Point: [" + PenX + ", " + PenY + "]  Width:" + Width + "  Height:" + Height);
        }

        public void Triangle_ValidSetting()
        {
            float Base = 50;
            float Adj = 50;
            float PenX = 100;
            float PenY = 100;
            Rectangle rectangle = new Rectangle();
            rectangle.Set(PenX, PenY, Base, Adj);
            Assert.AreEqual(rectangle.ToString(), "Assignment.Rectangle  Point: [" + PenX + ", " + PenY + "]  Base:" + Base + "  Adj:" + Adj);
        }

        public void Polygon_ValidSetting()
        {
            float[] points = {100, 100, 130, 120, 200, 50, 150, 0, 0, 100};
            float PenX = 100;
            float PenY = 100;
            float[] pts = new float[points.Length + 2];
            pts[0] = PenX;
            pts[1] = PenY;
            for (int i = 2; i < pts.Length; i++)
            {
                pts[i] = points[i - 2];
            }
            Polygon polygon = new Polygon();
            polygon.Set(pts);
            Assert.AreEqual(polygon.ToString(), "Assignment.Rectangle  Point: [" + PenX + ", " + PenY + "]  Location:[" + string.Join(",", pts) + "]");
        }
    }
}
