﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
   public class TempClass
    {
        private string[] a;
        private string name;
      public  TempClass(string name,string[] a)
        {
            this.a = a;
            this.name = name;
        }
        public void setA(string[] a)
        {
            this.a = a;
        }
        public string[] getA()
        {
            return a;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public string getName()
        {
            return name;
        }
       
    }
}
