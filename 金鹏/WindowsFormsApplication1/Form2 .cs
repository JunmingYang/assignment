﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        string t_flag = string.Empty;
        public static int x_flag = 0;
        public static int y_flag = 0;
        List<TempClass> list;
        public Form2(List<TempClass> list)
        {
            this.list = list;
            InitializeComponent();
           
        }
     
       
        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            x_flag = 0;
            y_flag = 0;
            foreach (TempClass t in list)
            {
                if (t.getName().IndexOf("MoveTo") >= 0)
                {
                    Form2.x_flag+= Convert.ToInt32(t.getA()[0]);
                    Form2.y_flag+= Convert.ToInt32(t.getA()[1]);
                }
                if (t.getName().IndexOf("Drawto") >= 0)
                {
                    Form2.x_flag += Convert.ToInt32(t.getA()[0]);
                    Form2.y_flag += Convert.ToInt32(t.getA()[1]);
                }
                if (t.getName().IndexOf("Line")>=0)
                {
                    Graphics g = this.CreateGraphics();
                    Pen p = new Pen(Color.Red, 2);
                   
                    g.DrawLine(p, x_flag,y_flag, Convert.ToInt32(t.getA()[0]),Convert.ToInt32(t.getA()[1]));
                    Form2.x_flag += Convert.ToInt32(t.getA()[0]);
                    Form2.y_flag += Convert.ToInt32(t.getA()[1]);
                }
                if (t.getName().IndexOf("circle") >= 0)
                {
                    Brush bush = new SolidBrush(Color.Green);//填充的颜色
                    Graphics g = this.CreateGraphics();
                    Pen p = new Pen(Color.Red, 2);
                    int x = Convert.ToInt32(t.getA()[0]);
                    g.DrawEllipse(p,x_flag,y_flag,x,x);
                }
                if (t.getName().IndexOf("Rectangle") >= 0)
                {
                    Graphics g = this.CreateGraphics();
                    Pen p = new Pen(Color.Red, 2);
                   
                    int x = Convert.ToInt32(t.getA()[0]);
                    int y = Convert.ToInt32(t.getA()[1]);
                    g.DrawRectangle(p,x_flag,y_flag,x,y);
                }
              
                if (t.getName().IndexOf("Triangle") >= 0)
                {
                    Pen pen = new Pen(Color.Red,1);
                    int x = Convert.ToInt32(t.getA()[0]);
                    int y = Convert.ToInt32(t.getA()[1]);
                    Graphics g = this.CreateGraphics();
                    g.DrawLine(pen, x_flag, y_flag, x + x_flag, y);
                    g.DrawLine(pen, x_flag, y_flag, x,y-x_flag);
                    g.DrawLine(pen, x, y - x_flag, x + x_flag, y);
                    // g.DrawLine(pen, x_flag, y_flag, x, y+y_flag);
                    Console.WriteLine(x_flag+" "+y_flag+"  "+x+" "+y);

                }
                if (t.getName().IndexOf("Polypon") >= 0)
                {

                    List<Point> p = new List<Point>();
                    Pen pen = new Pen(Color.Red, 1);
                    int x = Convert.ToInt32(t.getA()[0]);
                    int y = Convert.ToInt32(t.getA()[1]);
                    Graphics g = this.CreateGraphics();
                    p.Add(new Point(x_flag,y_flag));
                    for (int a=0; a<t.getA().Length;a++)
                    {
                        p.Add(new Point(Convert.ToInt32(t.getA()[a]) + x_flag, Convert.ToInt32(t.getA()[a+1]) + y_flag));
                        a++;
                    }
                    Point[] s=p.ToArray();
                    g.DrawPolygon(pen, s);
                }

                }
            

        }
    }
}
